from rest_framework import serializers
from ifc_to_gltf.models import Trasform
from django.contrib.auth.models import User

class TrasformSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    
    class Meta:
        model = Trasform
        fields = ['url', 'status', 'owner', 'start', 'complete', 
            'origin_file_name', 'origin_file_path', 'trasform_file_name', 'trasform_file_path']

class UserSerializer(serializers.HyperlinkedModelSerializer):
    trasforms = serializers.HyperlinkedRelatedField(many=True, view_name='trasforms-detail', read_only=True)
    
    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'trasforms']

