from django.contrib import admin, messages
from django.utils.translation import ugettext as _
import traceback
import sys

from .models import Trasform

def convert_action(modeladmin, request, queryset):
   for curr in queryset:
        try:
            curr.convert() 
            messages.info(request, _("Conversion completed"))
        except Exception as e:
            messages.error(request, _("Error during conversion: %s" % (sys.exc_info(), )))
            traceback.print_exc()

convert_action.short_description = _("Convert to GLTF")

class TrasformAdmin(admin.ModelAdmin):
    list_display = [ "user_detail", "project", "start", "status", ]

    list_filter = [ "user_detail", "project", "status", "start", ]
    
    readonly_fields = [ "status", ]

    actions = [ convert_action, ] 

admin.site.register(Trasform, TrasformAdmin)
