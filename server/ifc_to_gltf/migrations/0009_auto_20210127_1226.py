# Generated by Django 3.1.5 on 2021-01-27 12:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ifc_to_gltf', '0008_auto_20210127_1132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trasform',
            name='origin_url',
            field=models.URLField(blank=True, max_length=1000, null=True),
        ),
    ]
