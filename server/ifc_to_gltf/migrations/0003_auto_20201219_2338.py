# Generated by Django 3.1.3 on 2020-12-19 21:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ifc_to_gltf', '0002_auto_20201219_2334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='trasform',
            name='complete',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='trasform',
            name='start',
            field=models.DateTimeField(null=True),
        ),
    ]
