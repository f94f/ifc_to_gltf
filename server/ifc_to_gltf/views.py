from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from ifc_to_gltf.models import Trasform
from ifc_to_gltf.serializers import TrasformSerializer
from workflow.models import UserDetail, Project, Solution
from server import settings
from datetime import datetime
import json, sys, pytz, re, subprocess, threading, os

class TrasformViewSet(viewsets.ModelViewSet):
    queryset = Trasform.objects.all()
    serializer_class = TrasformSerializer

@csrf_exempt
def trasform_request(request, format=None):
    msg = "Non è stato possibile trasformare il file"
    try:
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        user_detail = UserDetail.objects.filter(pk=body['user_detail'], active=True).first()
        project = Project.objects.filter(pk=body['project'], active=True).first()
        
        # Verifico il json contenga informazione corretta
        if user_detail and project and body['origin_file_name'] and body['origin_file_name']:
            trasform = Trasform(user_detail=body['user_detail'], project=body['project'],
                origin_file_name=body['origin_file_name'], origin_file_path=body['origin_file_path'],
                trasform_file_name=body['trasform_file_name'], trasform_file_path=body['trasform_file_path'])
            # determino se il file si trova su un path o su una url
            code = "path"
            x = re.findall("http+|https+|www+|.com+|.org+|.edu+", body['origin_file_path'])
            if x:
                code = "url"
            trasform.save()
            
            # Chiamo un processo async per trasformare il file
            # msg = trasform_file(trasform, code)
            msg = trasform.convert()
            # msg = "Trasformado file"
            # thr = threading.Thread(target=trasform_file, args=(trasform, code), kwargs={})
            # thr.start()
    except:
        print("Unexpected error:", sys.exc_info()[0])
        msg = "Errore innespettato"
    return HttpResponse(msg)



