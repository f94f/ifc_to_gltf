from django.apps import AppConfig


class IfcToGltfConfig(AppConfig):
    name = 'ifc_to_gltf'
