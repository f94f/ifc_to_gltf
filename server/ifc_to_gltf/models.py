from django.conf import settings
from django.db import models
from workflow.models import UserDetail, Project, Solution
import os
import sys
import pytz
import subprocess
from datetime import datetime

##TRASFORM_STATUS = (
##    ("Created", "Created"),
##    ("Running", "Running"),
##    ("Done", "Done"),
##    ("Error", "Error"),
##)

class Trasform(models.Model):
#    worker =  models.CharField(max_length=100, blank=False, null=False)
#    project =  models.CharField(max_length=100, blank=False, null=False)
    user_detail = models.ForeignKey(UserDetail, on_delete=models.SET_NULL, blank=False, null=True)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, blank=False, null=True)
    
    created = models.DateTimeField(auto_now_add=True)
    start = models.DateTimeField(auto_now_add=False, blank=True, null=True)
    complete = models.DateTimeField(auto_now_add=False, blank=True, null=True)
    status = models.CharField(max_length=50, default='Created')

#    origin_file_name = models.CharField(max_length=100, blank=False, null=False)
#    origin_file_path = models.CharField(max_length=100, blank=False, null=False)


    origin_file = models.FileField(max_length=1000, blank=True, null=True)
    origin_url = models.URLField(max_length=1000, blank=True, null=True)
    trasform_file_name = models.CharField(max_length=100, blank=True, null=True)
    trasform_file_path = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        ordering = ['created']

    @property
    def origin_file_path(self):
        return os.path.dirname(self.origin_file.path)

    @property
    def origin_file_name(self):
        return os.path.basename(self.origin_file.path)

    def convert(self):
        msg = ""
        # Determina il folder dove salvare i file trasformati
        path_file = self.trasform_file_path
        if not self.trasform_file_path:
            os.makedirs(settings.WORKFLOW_BUILD_SOURCE_PATH + "/" + self.trasform_file_name, exist_ok=True)
            path_file = settings.WORKFLOW_BUILD_SOURCE_PATH + "/" + self.trasform_file_name
        
        if self.origin_url is not None and len(self.origin_url) > 0:
            origin = self.origin_url
            code = "url"
        elif self.origin_file:
            origin = self.origin_file.path
            code = "path"
        else:
            raise ValueError("You must specify a file path or an URL")
 

        self.start = datetime.utcnow().replace(tzinfo=pytz.utc)
       
        cmd_path = None

        if settings.TRASFORMER_EXE_PATH:
            cmd_path = settings.TRASFORMER_EXE_PATH

            # Determina l'esecuzione del Trasfomer e i suoi parametri
            app = "%s %s %s %s %s" % (settings.TRASFORMER_EXE_PATH, code, origin, path_file, self.trasform_file_name)
            
            print("Launch process: %s (working dir: %s)" % (app, settings.TRASFORMER_ROOT,))
            p = subprocess.Popen(app, cwd=settings.TRASFORMER_ROOT, shell=True)
            print("subprocess.Popen returned %s" % p)
            p.communicate()
            res = p.poll()

        else:
            print("No process specified. Simulate the conversion ...")
            res = 0


        if res != 0:
            # print(":(")
            self.complete = datetime.utcnow().replace(tzinfo=pytz.utc)
            self.status = "Error (%s)" % res
            msg = "Errore trasformando il file"
        else:
            # print(":)")
            self.complete = datetime.utcnow().replace(tzinfo=pytz.utc)
            self.status = "Complete"
            msg = "Il file è stato trasformato"
            self.create_solution(path_file)
        self.save()
        return msg
    
    def create_solution(self, path_file):
#        project = Project.objects.filter(pk=self.project, active=True).first()
    
        solution = Solution(project=self.project, name=self.trasform_file_name, 
            url_ifc = self.origin_file_path,
            path_ifc = self.origin_file_path,
            path_gltf = os.path.join(path_file, self.trasform_file_name + ".gltf"),
            path_json = os.path.join(path_file, self.trasform_file_name + ".json"))
        solution.save()
        solution.url_gltf = "file/" + str(solution.pk) + "/gltf"
        solution.url_json = "file/" + str(solution.pk) + "/json"
        solution.save()
