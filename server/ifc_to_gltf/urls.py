from django.urls import path, include
from rest_framework import routers
from ifc_to_gltf import views

router = routers.DefaultRouter()
router.register('trasform', views.TrasformViewSet)

urlpatterns = [
    path('trasform', include(router.urls)),
    path('ifc_to_gltf/', views.trasform_request),
]