"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from workflow.urls import router as router_workflow
from nfc.urls import router as router_nfc

from rest_framework import routers

router = routers.DefaultRouter()
router.registry.extend(router_workflow.registry)
router.registry.extend(router_nfc.registry)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('ifc_to_gltf/', include('ifc_to_gltf.urls')),
    path('workflow/', include('workflow.urls')),
    path('nfc/', include('nfc.urls')),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)