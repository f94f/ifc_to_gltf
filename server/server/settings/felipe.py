from server.settings.default import *

DEBUG = True


TRASFORMER_ROOT = os.path.join("D:\\", "Temp", "tesi")

TRASFORMER_EXE_FOLDER = os.path.join("D:\\", "Users", "Felipe", "Documents", "universita", "Tesi", "trasformer_v2", "Trasformer", "Trasformer", "bin", "Debug", "net47")
TRASFORMER_EXE_PATH = os.path.join(TRASFORMER_EXE_FOLDER, "Trasformer.exe")
#TRASFORMER_EXE_PATH = None
WORKFLOW_BUILD_SOURCE_PATH = os.path.join(TRASFORMER_ROOT, "data")
WORKFLOW_BUILD_COMMENT_PATH = os.path.join(TRASFORMER_ROOT, "comments")

STATIC_ROOT = os.path.join(TRASFORMER_ROOT, "static")
MEDIA_ROOT = os.path.join(TRASFORMER_ROOT, "media")