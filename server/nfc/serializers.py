from rest_framework import serializers
from nfc.models import NFCReader
from django.contrib.auth.models import User

class NFCReaderSerializer(serializers.HyperlinkedModelSerializer):    
    class Meta:
        model = NFCReader
        fields = ['url', 'id_tag', 'user_detail', 'created_at']
