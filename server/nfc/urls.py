from django.urls import path, include
from rest_framework import routers
from nfc import views

router = routers.DefaultRouter()
router.register('nfcreader', views.NFCReaderViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('index/', views.nfc_reader),
    #path('send/<int:worker>/<int:tag>/', views.send_nfc),
    #path('get/<int:worker>/', views.get_nfc),
]