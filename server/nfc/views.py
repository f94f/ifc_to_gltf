import sys
import json
from django.conf import settings
from django.template import loader
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from nfc.models import NFCReader
from nfc.serializers import NFCReaderSerializer
from workflow.models import UserDetail

class NFCReaderViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = NFCReader.objects.all()
    serializer_class = NFCReaderSerializer
    filter_backends = [DjangoFilterBackend]
    #filterset_fields = ['user_detail']

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request.user.is_authenticated:
            return queryset.filter(user_detail__user=self.request.user)
        else:
            return queryset.none()

def nfc_reader(request):
    context = {'nfc': ''}
    template = loader.get_template('nfc/index.html')
    return HttpResponse(template.render(context, request))

def get_nfc(request, user_detail, format=None):
    """
    Recive user_detail id
    """
    try:
        nfcReader = NFCReader.objects.filter(user_detail_id=user_detail).latest('created_at')
    except NFCReader.DoesNotExist:
        print("Unexpected error:", sys.exc_info()[0])
        return HttpResponse(status=404)
    
    data = { 'id': nfcReader.id_tag, 'description': '', 'date': str(nfcReader.created_at) }
    return HttpResponse(json.dumps(data), content_type="application/json")