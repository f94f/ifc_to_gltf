from django.db import models
from workflow.models import UserDetail

class NFCReader(models.Model):
    id_tag = models.CharField(max_length=200)
    user_detail = models.ForeignKey(UserDetail, related_name='readings', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)