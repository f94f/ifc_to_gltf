from django.contrib import admin

from .models import NFCReader

admin.site.register(NFCReader)