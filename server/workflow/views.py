import sys
import os
import base64
from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.parsers import JSONParser
from rest_framework import viewsets
from workflow.models import UserDetail, Project, Solution, Folder, Expression, Comment
from workflow.serializers import UserDetailSerializer, ProjectSerializer, FolderSerializer, SolutionSerializer, CommentSerializer
from django.db import transaction

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

class SolutionViewSet(viewsets.ModelViewSet):
    queryset = Solution.objects.all()
    serializer_class = SolutionSerializer

class UserDetailViewSet(viewsets.ModelViewSet):
    queryset = UserDetail.objects.all()
    serializer_class = UserDetailSerializer

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request.user.is_authenticated:
            return queryset.filter(user=self.request.user)
        else:
            return queryset.none()

class FolderViewSet(viewsets.ModelViewSet):
    queryset = Folder.objects.all()
    serializer_class = FolderSerializer

class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['user_detail']

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.request.user.is_authenticated:
            qset = queryset.filter(user_detail__user=self.request.user)

            sol = self.request.query_params.get('solution', None)
            obj = self.request.query_params.get('obj', None)

            if sol is not None:
                qset = queryset.filter(user_detail__user=self.request.user, solution=sol)
            if obj is not None:
                qset = queryset.filter(user_detail__user=self.request.user, id_ref_obj=obj)
            
            return qset
        else:
            return queryset.none()

    def create(self, request):
        data = request.data

        try:
            solution = Solution.objects.filter(pk=data["solution"], active=True).first()
            user_det = UserDetail.objects.filter(user=request.user, active=True).first()
        except:
            return HttpResponse(status=404)

        try:
            with transaction.atomic():
                com = Comment(solution=solution, user_detail=user_det, id_ref_obj=data["id_ref_obj"])
                com.save()

                name_comment = str(data["id_ref_obj"]) + '_' + str(com.id) + '.wav'

                com.path_audio = "comments/" + str(data["solution"]) + "/" + name_comment
                com.url_audio = "audio/" + str(com.id)
                com.save()

                # os.makedirs(settings.WORKFLOW_BUILD_COMMENT_PATH + "/" + str(data["solution"]), exist_ok=True)
                # path_file = settings.WORKFLOW_BUILD_COMMENT_PATH + "/" + str(data["solution"]) # path.join(..., ..., ...)

                os.makedirs(os.path.join(settings.WORKFLOW_BUILD_COMMENT_PATH, str(data["solution"])), exist_ok=True)
                path_file = os.path.join(settings.WORKFLOW_BUILD_COMMENT_PATH, str(data["solution"]))
        
                if data["audio"] is None:
                    raise Exception("No audio found")

                byte_audio = base64.b64decode(data["audio"])

                with open(path_file + '/' +  name_comment, "wb") as f:
                    f.write(byte_audio)

            return HttpResponse({'status : success, desc : Comment created successfull'})
        except Exception as error:
            print(sys.exc_info())
            return HttpResponse({'status : fail, desc : Internal error, detail: "' + error.message + '"'})

def get_file(request, pk, ext, format=None):
    """
    Retrieve file
    """
    try:
        solution = Solution.objects.filter(pk=pk, active=True).first()
    except Solution.DoesNotExist:
        return HttpResponse(status=404)
    
    file_data = None
    if ext == "ifc":
        file_data = open(os.path.join(settings.TRASFORMER_ROOT, solution.path_ifc), "rb").read()
    elif ext == "gltf":
        file_data = open(os.path.join(settings.TRASFORMER_ROOT, solution.path_gltf), "rb").read()
    elif ext == "json":
        file_data = open(os.path.join(settings.TRASFORMER_ROOT, solution.path_json), "rb").read()

    return HttpResponse(file_data, content_type=ext)

def get_audio(request, pk, format=None):
    """
    Retrieve audio
    """
    print ("****************************")
    try:
        comment = Comment.objects.filter(pk=pk, active=True).first()
    except Comment.DoesNotExist:
        return HttpResponse(status=404)
    print ("----------------------------")
    file_data = open(os.path.join(settings.TRASFORMER_ROOT, comment.path_audio), "rb")
    print(file_data)

    response = HttpResponse()
    response.write(file_data.read())
    response['Content-Type'] ='audio/wav'
    response['Content-Length'] = os.path.getsize(os.path.join(settings.TRASFORMER_ROOT, comment.path_audio))
    return response

# @csrf_exempt
# def ask_project(request, pk):
#     """
#     Retrieve project
#     """
#     try:
#         project = Project.objects.filter(pk=pk, active=True).first()
#     except Project.DoesNotExist:
#         return HttpResponse(status=404)

#     serializer_context = {
#         'request': request,
#     }

#     if request.method == 'GET':
#         serializer = ProjectSerializer(project, context=serializer_context)
#         return JsonResponse(serializer.data)

# @csrf_exempt
# def ask_solution(request, pk):
#     """
#     Retrieve solution
#     """
#     try:
#         solution = Solution.objects.filter(pk=pk, active=True).first()
#     except Solution.DoesNotExist:
#         return HttpResponse(status=404)

#     if request.method == 'GET':
#         serializer = SolutionSerializer(solution)
#         return JsonResponse(serializer.data)