from rest_framework import serializers
from workflow.models import UserDetail, Project, Solution, Folder, Expression, Comment

class ExpresionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expression
        fields = ['value']

class FolderSerializer(serializers.HyperlinkedModelSerializer):
    expressions = ExpresionSerializer(many=True, read_only=True)
    class Meta:
        model = Folder
        fields = ['url', 'name', 'expressions']

class SolutionSerializer(serializers.HyperlinkedModelSerializer):
    url_gltf = serializers.SerializerMethodField(method_name='calculate_url_gltf')
    url_json = serializers.SerializerMethodField(method_name='calculate_url_json')

    class Meta:
        model = Solution
        fields = ['url', 'id', 'name','url_ifc', 'url_gltf', 'url_json']
    
    def calculate_url_gltf(self, instance):
        return self.context['request'].build_absolute_uri('/') + "workflow/" + instance.url_gltf
    
    def calculate_url_json(self, instance):
        return self.context['request'].build_absolute_uri('/') + "workflow/" + instance.url_json


class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    solutions = SolutionSerializer(many=True, read_only=True)
    
    class Meta:
        model = Project
        fields = ['url', 'id', 'name', 'active', 'solutions']

class UserDetailSerializer(serializers.ModelSerializer):
    projects = ProjectSerializer(many=True, read_only=True)
    
    class Meta:
        model = UserDetail
        fields = ['url', 'id', 'name', 'identify', 'projects']

class CommentSerializer(serializers.HyperlinkedModelSerializer):
    url_audio = serializers.SerializerMethodField(method_name='calculate_audio_path')

    class Meta:
        model = Comment
        fields = ['url', 'id', 'user_detail', 'solution', 'created_at', 'id_ref_obj', 'url_audio']

    def calculate_audio_path(self, instance):
        return self.context['request'].build_absolute_uri('/') + "workflow/" + str(instance.url_audio)