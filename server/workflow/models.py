from django.db import models
from django.contrib.auth.models import User

class UserDetail(models.Model):
    name = models.CharField(max_length=200)
    identify = models.TextField()
    active = models.BooleanField(default=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

class Project(models.Model):
    user_detail = models.ForeignKey(UserDetail, related_name='projects', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    active = models.BooleanField(default=True)
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

class Solution(models.Model):
    project = models.ForeignKey(Project, related_name='solutions', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    url_ifc = models.CharField(max_length=200, null=True, blank=True)
    url_gltf = models.CharField(max_length=200, null=True, blank=True)
    url_json = models.CharField(max_length=200, null=True, blank=True)
    path_ifc = models.CharField(max_length=200, null=True, blank=True)
    path_gltf = models.CharField(max_length=200, null=True, blank=True)
    path_json = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name

class Comment(models.Model):
    solution = models.ForeignKey(Solution, related_name='comments', on_delete=models.CASCADE)
    user_detail = models.ForeignKey(UserDetail, related_name='user_comments', on_delete=models.CASCADE, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    id_ref_obj = models.CharField(max_length=200, null=True, blank=True)
    url_audio = models.CharField(max_length=200, null=True, blank=True)
    path_audio = models.CharField(max_length=200, null=True, blank=True)
    active = models.BooleanField(default=True)
    def __str__(self):
        return self.id_ref_obj

# Struttura della suddivisione degli elementi in categorie
class Folder(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Expression(models.Model):
    folder = models.ForeignKey(Folder, related_name='expressions', on_delete=models.CASCADE)
    value = models.CharField(max_length=200)
    def __str__(self):
        return self.value