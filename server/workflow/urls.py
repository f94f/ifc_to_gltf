from django.urls import path, include
from rest_framework import routers
from workflow import views

router = routers.DefaultRouter()
router.register('user_detail', views.UserDetailViewSet)
router.register('project', views.ProjectViewSet)
router.register('folder', views.FolderViewSet)
router.register('solution', views.SolutionViewSet)
router.register('comment', views.CommentViewSet)
router.register(r'comment/(?P<solution>\w+)', views.CommentViewSet, basename='Comment-solution')
router.register(r'comment/(?P<solution>\w+)/(?P<obj>\w+)', views.CommentViewSet, basename='Comment-obj')

urlpatterns = [
    path('', include(router.urls)),
    path('file/<int:pk>/<str:ext>/', views.get_file),
    path('audio/<int:pk>/', views.get_audio),
]