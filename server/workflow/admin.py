from django.contrib import admin

from .models import UserDetail, Project, Solution, Folder, Expression, Comment

admin.site.register(UserDetail)
admin.site.register(Project)
admin.site.register(Solution)
admin.site.register(Folder)
admin.site.register(Expression)
admin.site.register(Comment)