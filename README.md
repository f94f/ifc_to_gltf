# Descrizzione
Server utilizzato nel progetto [ODAVS](https://gitlab.com/univpm.dicea.bc/odavs). Questo server permette di gestire il collegamento tra i file ifc caricati da un esperto e la visualizzazione di esso modello nel luego di costruzione, usando gli occhiali HoloLens.

# Build the application
Il server è stato sviluppato usando [Python 3.7](https://www.python.org/), sul sistema operativo Windows 10.

Le librerie utilizzate sono le seguenti:
1. Django
2. Django REST Framework
3. 

## Installazione
1. Installare Python.
2. Installare le librerie.
3. Aggiungere un file di configurazione Settings o cambiare quello per default, impostando correttamente i seguenti parametri:
    *. BUILD_PATH: Il path dove è a disposizione il progetto di tradizzione dei file ifc [Python 3.7](https://gitlab.com/f94f/trasformer)
    *.

# Tutoriale
## Pagina principale
1. Nella pagina principale vengono mostrati tutti i dati presenti nel DB e il link da dove tirarli fuori.
2. Ogni pagina è un richiamo REST dei dati il quale ottiene come risposta un Json con i dati chiesti.

## Sezzione administrativa
1. Per entrare nella sezzione administrativa, bisogna effetturale il login ed andare nella URL SERVER_URL/admin.
2. Qui dentro viene mostrato ogni modello dentro il DB e i suoi dati ed è possibile anche aggiungere nuovi dati.

## Traduzzione file IFC a GLTF
1. Dentro la schermata administrativa, bisogna selezzionare dentro la categoria ifc_to_gltf il modello Trasformer e creare uno nuovo.
2. Aggiungere i dati corrispondenti, come anche caricare il file ifc e salvare.
3. Nella schermata dove compargono tutti i Trasformer, scegliere quello quello a cui verrà generato i file GLTF e Json, dopo scegliere la opzione di traduzione dal drop down lis in alto e avviare il processo.
4. Alla fine del processo verrà generato anche la Soluzione (il modello 3D) corrispondente.

## Categorizzaione
1. Dentro Folder viene definita la categoria.
2. Per un Folder vengono assegnati l'espressioni regolari corrispondenti.
3. Le espressioni verranno usate per determinare se un oggetto appartiene o no alla categoria.

## NFC Readers
1. Qui vengno riportate le ultime letture fattedi un codice NFC.
2. L'ultima lettura per l'utente corrispondete viene richiesta dal HoloLens per fare l'allineamento.

## Workstation
1. Serve per categorizzare l'appartenenza di un proggetto a un luogo.
2. Dentro ci sono i progetti con i modelli 3D.
